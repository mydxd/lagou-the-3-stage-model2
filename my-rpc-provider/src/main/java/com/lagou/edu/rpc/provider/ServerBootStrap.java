package com.lagou.edu.rpc.provider;

import com.lagou.edu.rpc.common.ConfigKeeper;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(value = "com.lagou.edu")
@SpringBootApplication
public class ServerBootStrap {

    private static final String ZOOKEEPER_ADDRESS = "47.93.232.149:2181";

    public static void main(String[] args) {
        int port = 8990;
        if (args.length > 0 && NumberUtils.isDigits(args[0])) {
            port = Integer.parseInt(args[0]);
        }

        ConfigKeeper configKeeper = ConfigKeeper.getInstance();
        configKeeper.setPort(port);
        configKeeper.setZkAddr(ZOOKEEPER_ADDRESS);
        configKeeper.setProviderSide(true);

        SpringApplication.run(ServerBootStrap.class, args);
    }
}
