package com.lagou.edu.rpc.api;

public interface UserService {
    /**
     * 打招呼
     *
     * @param word
     * @return
     */
    String sayHello(String word);
}
